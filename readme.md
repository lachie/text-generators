# 👏🎺
A react app to house all my text generators

view live at http://txt.lachie.co

## Features
- :clap: Replaces spaces with a :clap:
- :trumpet: Subsitutes half width characters for full width characters to make things ａｅｓｔｈｅｔｉｃ
- :b: Replaces the character b or the first character of any word longer than 3 characters with a :b:
- :game_die: Generates a random emoji

## Made Using
- My [react boilerplate](https://github.com/alachie/react-boilerplate)
- React
- React Router
- Redux 
- Webpack
- Sass
- [this emoji list](https://github.com/chazgiese/EmojiGen/blob/master/emojigen.js)
